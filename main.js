const tabs=document.getElementById('tabs');
const contents=document.querySelectorAll('.content');

function changeClass(ev) {
for(let i=0; i<tabs.children.length;i++){
tabs.children[i].classList.remove('active');
}
ev.classList.add('active');
}

tabs.addEventListener('click',function(event){
    changeClass(event.target);
    const currT=event.target.dataset.btn;
    for( i=0; i<contents.length;i++){
        contents[i].classList.remove('active');
        
        if(contents[i].dataset.content===currT) {
    contents[i].classList.add('active');
        }
    }
})


  
